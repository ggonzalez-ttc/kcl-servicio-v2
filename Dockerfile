FROM amazoncorretto:11

ENV JAVA_OPTS=""
ENV APP_HOME="/app"

ARG PYTHON_VERSION=3.6.4
ARG BOTO3_VERSION=1.6.3
ARG BOTOCORE_VERSION=1.9.3
ARG APP_USER=app

RUN yum -y install python-pip

RUN pip install awscli --upgrade --user

# Crear carpetas de la aplicación
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/config
RUN mkdir $APP_HOME/log

VOLUME $APP_HOME/log
VOLUME $APP_HOME/config

COPY start.sh $APP_HOME/start.sh
RUN chmod 700 $APP_HOME/start.sh

COPY target/*.jar $APP_HOME/app.jar

# $APP_HOME/application.properties sera copiado al inicial el contenedor
RUN touch $APP_HOME/application.properties

ENTRYPOINT [ "sh", "-c", "$APP_HOME/start.sh"]
