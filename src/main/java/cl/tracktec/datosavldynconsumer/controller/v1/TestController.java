package cl.tracktec.datosavldynconsumer.controller.v1;


import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/test")
@Slf4j
@Getter
public class TestController {


    @Value("${tabla.streams}")
    private String url;



    @GetMapping(value = "/")
    public ResponseEntity<List<String>> getTest() {

        log.info("test-{}", url);

        List<String> result = new ArrayList();

        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @GetMapping(value = "/streams")
    public ResponseEntity<List<String>> getStreams() {

        log.info("test-{}", url);

        List<String> result = new ArrayList();



        log.info("Done. getStreams");



        return new ResponseEntity<>(result, HttpStatus.OK);
    }





}
