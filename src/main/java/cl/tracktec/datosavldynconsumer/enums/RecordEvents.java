package cl.tracktec.datosavldynconsumer.enums;

import lombok.Getter;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RecordEvents {

	// desde com.amazonaws.services.dynamodbv2.model.Record

	INSERT("INSERT"),
	MODIFY("MODIFY"),
	REMOVE("REMOVE");

    @Getter private String value;
}


