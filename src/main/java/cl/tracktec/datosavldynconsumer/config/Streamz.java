package cl.tracktec.datosavldynconsumer.config;


import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreams;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreamsClientBuilder;
import com.amazonaws.services.dynamodbv2.streamsadapter.AmazonDynamoDBStreamsAdapterClient;
import com.amazonaws.services.dynamodbv2.streamsadapter.StreamsWorkerFactory;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import cl.tracktec.datosavldynconsumer.enums.RecordEvents;
import cl.tracktec.datosavldynconsumer.kinesis.v2.helper.ddl.StreamsAdapterDemoDdlHelper;
import cl.tracktec.datosavldynconsumer.kinesis.v2.processor.StreamsRecordProcessorFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Streamz {

	@Value("${startup.sleep}")
	private int startupSleep;

	@Value("${tabla.streams}")
	private String tablaStreams;

	@Value("${nombre.aplicacion}")
	private String nombreAplicacion;  //se crea tabla en dynamodb

	@Value("${worker.id}")
	private String workerId;

	@Value("${worker.number}")
	private int workerNumber;

	// TODO: autowired
	private static Worker worker;
	private static KinesisClientLibConfiguration workerConfig;
	private static IRecordProcessorFactory recordProcessorFactory;

	private static AmazonDynamoDB dynamoDBClient;
	private static AmazonCloudWatch cloudWatchClient;
	private static AmazonDynamoDBStreams dynamoDBStreamsClient;
	private static AmazonDynamoDBStreamsAdapterClient adapterClient;

	private static AWSCredentialsProvider awsCredentialsProvider = DefaultAWSCredentialsProviderChain.getInstance();

	private static Regions awsRegion = Regions.US_WEST_2; // Oregon

	// final Executor executor;
	private ExecutorService executorService;
	private final List<Worker> workers = new LinkedList<>();

	// DynamoDB obtener streams
	public void getStreams() {

		log.info("Starting demo...{}/{}/{}", RecordEvents.INSERT.getValue(), RecordEvents.MODIFY.getValue(), RecordEvents.REMOVE.getValue());

		log.info("startupSleep: {}", startupSleep);
		log.info("tablaStreams: {}", tablaStreams);
		log.info("nombreAplicacion: {}", nombreAplicacion);
		log.info("workerId: {}", workerId);
		log.info("workerNumber: {}", workerNumber);

		// TODO: estos builders deberia ser beans.
		dynamoDBClient = AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion).build();
		cloudWatchClient = AmazonCloudWatchClientBuilder.standard().withRegion(awsRegion).build();
		dynamoDBStreamsClient = AmazonDynamoDBStreamsClientBuilder.standard().withRegion(awsRegion).build();
		adapterClient = new AmazonDynamoDBStreamsAdapterClient(dynamoDBStreamsClient);
		recordProcessorFactory = new StreamsRecordProcessorFactory(dynamoDBClient, tablaStreams);
		// obtener el ultimo stream arn
		String streamArn = StreamsAdapterDemoDdlHelper.describeTable(dynamoDBClient, tablaStreams).getTable().getLatestStreamArn();

		log.info("1Creating worker for stream: ", streamArn);

		workerConfig = new KinesisClientLibConfiguration(nombreAplicacion,		// applicationName
														 streamArn, 			// streamName
														 awsCredentialsProvider,// credentialsProvider
														 workerId) 				// workerId
						.withMaxRecords(1000).withIdleTimeBetweenReadsInMillis(500)
						// change the behavior of the record processors so that it always reads data
						// from the beginning of the stream:
						.withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON);


		executorService = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().
				                    setNameFormat("KinesisCollector-" + streamArn + "-%d").build());

		//TODO: generamos workers
		//for (int i=0; i<workerNumber; i++) {

			worker = StreamsWorkerFactory.createDynamoDbStreamsWorker(recordProcessorFactory, workerConfig, adapterClient,
																		dynamoDBClient, cloudWatchClient);
			log.info("Starting worker...", 0); //i

			workers.add(worker);
			executorService.execute(worker);

		//}

		// espera creacion de resources.
		try {
			Thread.sleep(startupSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		log.info(">>> Streamz (executor) Done.");

	}

	@PreDestroy
	public void destruccion() {

		log.info("Streamz destruccion.................");

		workers.forEach(Worker::shutdown);
		// posible exception, falso positivo: Worker.run caught exception, sleeping for 500 milli seconds!.

		workers.clear();

		executorService.shutdownNow();

		log.info("Kinesis workers shutdown");

	}

}

