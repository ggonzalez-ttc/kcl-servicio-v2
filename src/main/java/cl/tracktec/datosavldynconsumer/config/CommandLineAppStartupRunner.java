package cl.tracktec.datosavldynconsumer.config;

import javax.annotation.PreDestroy;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CommandLineAppStartupRunner implements CommandLineRunner {

	private Streamz streamz;

	CommandLineAppStartupRunner(Streamz streamz) {
		this.streamz = streamz;
	}

	@Override
	public void run(String... args) throws Exception {

		// CORE del startup de Streamz!
		streamz.getStreams();

	}

	@PreDestroy
	public String destruccion() {

		log.info("Commandline destruccion.................");

		return "";
	}

}
