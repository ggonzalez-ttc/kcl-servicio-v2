package cl.tracktec.datosavldynconsumer.kinesis.v2.processor;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.streamsadapter.model.RecordAdapter;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.ShutdownReason;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
//import com.amazonaws.services.kinesis.model.Record;

import cl.tracktec.datosavldynconsumer.enums.RecordEvents;
import cl.tracktec.datosavldynconsumer.service.PipelineService;
import cl.tracktec.datosavldynconsumer.utils.WebHelper;
import kong.unirest.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

//import org.springframework.beans.factory.annotation.Value;


@Slf4j
public class StreamsRecordProcessor implements IRecordProcessor {

	//@Value("${checkpoint.frequency}")
	//TODO: no me esta inyectando... claro que no es @Component, ni @Service
	private int checkpointFrequency;

	private int checkpointCounter;

	private final AmazonDynamoDB dynamoDBClient;
	private final String tableName;

	public StreamsRecordProcessor(AmazonDynamoDB dynamoDBClient2, String tableName) {
		this.dynamoDBClient = dynamoDBClient2;
		this.tableName = tableName;

		 Properties prop = WebHelper.getProps();
		 checkpointFrequency = Integer.parseInt( (String) prop.get("checkpoint.frequency"));
	}

	@Override
	public void initialize(InitializationInput initializationInput) {
		checkpointCounter = 0;

		//TESTING:
		/*if (checkpointCounter==0) {
			Map test = new HashMap();
			test.put("hola", "mundo");
			test.put("foo", "bar");
			test.put("qux", "foobar");
			sendEvent(test);
		}*/
	}

	@Override
	public void processRecords(ProcessRecordsInput processRecordsInput) {

		// recorre y parsea mensajes recibidos
		for (com.amazonaws.services.kinesis.model.Record record : processRecordsInput.getRecords()) {

			log.info("processRecords()...");

			//solo para log, podemos remover
			//String data = new String(record.getData().array(), Charset.forName(StandardCharsets.UTF_8.toString()));
			log.info(new String(record.getData().array(), Charset.forName(StandardCharsets.UTF_8.toString())));


			if (record instanceof RecordAdapter) {

				//warning: tenemos 2 clases llamadas Record!
				com.amazonaws.services.dynamodbv2.model.Record streamRecord = ((RecordAdapter) record).getInternalObject();

				//solo esperamos INSERT
				if (RecordEvents.INSERT.toString().equals(streamRecord.getEventName())) {

					log.debug("INSERT found...");

					Map newImage = streamRecord.getDynamodb().getNewImage();
					log.debug("INSERT found... {}", newImage);

					//llama a cliente rest (-->pipeline)
					sendEvent(newImage);
				}
			}

			// TODO: necesitamos grupos mas grandes de 10 ?
			// you may need to increase this value from the default of 10 to avoid throttling.
			checkpointCounter += 1;
			if (checkpointCounter % checkpointFrequency == 0) {
				try {
					processRecordsInput.getCheckpointer().checkpoint();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void shutdown(ShutdownInput shutdownInput) {

		if (shutdownInput.getShutdownReason() == ShutdownReason.TERMINATE) {
			try {
				shutdownInput.getCheckpointer().checkpoint();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}



	public void sendEvent(Map newImage) {

		JSONObject json = new JSONObject(newImage);
		log.debug("json = {}", json);


    	//No puedo inyectarlo por Spring container porque el worker es creado por el builder createDynamoDbStreamsWorker
		PipelineService pipelineService = new PipelineService();

    	int pipeline = pipelineService.sendEvent(json);

    	log.debug("respuesta = {}", pipeline);

	}

}

