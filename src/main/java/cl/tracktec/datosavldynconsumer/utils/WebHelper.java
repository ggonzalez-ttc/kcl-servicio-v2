package cl.tracktec.datosavldynconsumer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;

/**
 * WebHelper
 *
 * Helper para dar solucion a los problemas de los builders de aws sdk
 * que no se pueden inyectar de forma natural en el DI container.
 *
 */

@Slf4j
public class WebHelper {

	public static Properties getProps() {

		 try (InputStream input = WebHelper.class.getClassLoader().getResourceAsStream("application.properties")) {

	         Properties prop = new Properties();

	         if (input == null) {
	             log.error("Sorry, unable to find application.properties !!!!!!!!!!!!!!!!!!!!!!!!!!!");
	             return null;  //anti-pattern!
	         }

	         prop.load(input);

	         //log.debug("****** checkpoint.frequency = {}", prop.getProperty("checkpoint.frequency"));
	         //log.debug("****** pipeline.url = {}", prop.getProperty("pipeline.url"));

	         return prop;

	     } catch (IOException ex) {
	         ex.printStackTrace();
	     }

		 return null;   //anti-pattern!
	}

}
