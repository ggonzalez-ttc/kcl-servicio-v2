package cl.tracktec.datosavldynconsumer.service;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cl.tracktec.datosavldynconsumer.utils.WebHelper;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PipelineService {

	//TODO: no me esta inyectando !
	//@Value("${pipeline.url}")
	private String pipelineUrl;

	public PipelineService() {
		log.info("creado!");

		Properties prop = WebHelper.getProps();
		pipelineUrl =  (String) prop.get("pipeline.url");

	}

	public int sendEvent(JSONObject json) {

		/* TODO:
		 Unirest.config()
           .socketTimeout(500)
           .connectTimeout(1000)
   		   .con.setConnectTimeout(5000);
		   .con.setReadTimeout(5000);


           .concurrency(10, 5)
           .proxy(new Proxy("https://proxy"))
           .setDefaultHeader("Accept", "application/json")
           .followRedirects(false)
           .enableCookieManagement(false)
           .addInterceptor(new MyCustomInterceptor());

		 */


        log.info("enviando json a Pipeline endpoint: {}", json);
        log.info("pipelineUrl: {}", pipelineUrl);


        //deberia ser POST???? podria ser por tamaño del mensaje ?
        //TODO: remover este comentario para poder llamar a endpoint de Pipeline:
        /*       HttpResponse<JsonNode> response = Unirest.get(pipelineUrl).asJson();

        log.info("response.getStatus = {}", response.getStatus());
        log.info("response.getStatusText = {}", response.getStatusText());
        log.info("response.getBody = {}", response.getBody());
        log.info("response.getCookies = {}", response.getCookies());
        log.info("response.getHeaders = {}", response.getHeaders()); */

        //aguante C.
        return 1;
	}

}

