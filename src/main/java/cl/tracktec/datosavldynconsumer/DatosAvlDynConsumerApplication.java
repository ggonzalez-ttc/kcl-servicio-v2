package cl.tracktec.datosavldynconsumer;

import javax.annotation.PreDestroy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class DatosAvlDynConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatosAvlDynConsumerApplication.class, args);

	}

	/*@Bean
	public String buildAmazonKinesis() {

		log.info("Bean testing creacion.................");

		return "built";
	}

	@PreDestroy
	public void destruccion() {

		log.info("Bean destruccion.................");

	}*/

}
