echo "Iniciando aplicación en $APP_HOME"
echo "JAVA_OPTS: $JAVA_OPTS"
echo "AMBIENTE: $AMBIENTE"
cd "$APP_HOME" || exit
/root/.local/bin/aws s3 cp s3://configuraciones-servicios-$AMBIENTE/datos-avl-dyn-consumer/application.properties $APP_HOME
java \
  $JAVA_OPTS \
  -Djava.security.egd=file:/dev/./urandom \
  -jar $APP_HOME/app.jar \
  --spring.config.location=$APP_HOME/application.properties
